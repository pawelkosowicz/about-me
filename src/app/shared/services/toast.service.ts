import { Injectable } from '@angular/core'
import { ToastrService } from 'ngx-toastr'

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  private readonly TIMEOUT = 3000

  constructor(private toastr: ToastrService) {}

  public showSuccesPopup(title: string, content: string) {
    this.toastr.success(title, content, {
      timeOut: this.TIMEOUT,
      positionClass: 'toast-bottom-right',
    })
  }

  public showErrorsPopup(title: string, content: string) {
    this.toastr.error(title, content, {
      timeOut: this.TIMEOUT,
      positionClass: 'toast-bottom-right',
    })
  }
}
