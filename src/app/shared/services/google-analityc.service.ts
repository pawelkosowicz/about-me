import { Injectable } from '@angular/core'
import {
  GAnalitycEventEnum,
  GAnalitycModuleEnum,
} from '../enums/gAnalityc.enum'

declare let gtag: any

@Injectable({
  providedIn: 'root',
})
export class GoogleAnalitycService {
  constructor() {}

  public sendEvent(
    eventModule: GAnalitycModuleEnum,
    event: GAnalitycEventEnum,
    label: string
  ) {
    gtag('event', event, {
      eventCategory: eventModule,
      eventLabel: label,
      eventAction: event,
      eventValue: 0,
    })
  }
}
