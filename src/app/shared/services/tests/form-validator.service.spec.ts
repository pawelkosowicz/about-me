import { TestBed } from '@angular/core/testing';

import { FormValidatorService, validatorErrors } from '../form-validator.service';
import { FormControl } from '@angular/forms';

describe('FormValidatorService', () => {

  let formValidatorService: FormValidatorService;
  beforeEach(()=> {
    TestBed.configureTestingModule({
      providers: [
        FormValidatorService
      ]
    })

    formValidatorService = TestBed.get(FormValidatorService);

  })

  it('should be created', () => {
    const service: FormValidatorService = TestBed.get(FormValidatorService);
    expect(service).toBeTruthy();
  });

  it('field should be required', () => {
    const control = new FormControl();
    control.setErrors({required: true});
    const result = formValidatorService.validateField(control);

    expect(typeof(result)).toBe('string');
    expect(result).toBe(validatorErrors.required);
  })

  it('field should has min length', () => {
    const control = new FormControl();
    control.setErrors({minlength: {
      requiredLength: 5
    }});
    const result = formValidatorService.validateField(control);

    expect(typeof(result)).toBe('string');
    expect(result).toBe(validatorErrors.minLength('5'));
  })

  it('field should be emial type', () => {
    const control = new FormControl();
    control.setErrors({email: true});
    const result = formValidatorService.validateField(control);

    expect(typeof(result)).toBe('string');
    expect(result).toBe(validatorErrors.email);
  })




});
