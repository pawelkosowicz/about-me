import { TestBed } from '@angular/core/testing';

import { GoogleAnalitycService } from '../google-analityc.service';

describe('GoogleAnalitycService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoogleAnalitycService = TestBed.get(GoogleAnalitycService);
    expect(service).toBeTruthy();
  });
});
