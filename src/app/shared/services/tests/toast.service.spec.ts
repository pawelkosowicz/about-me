import { TestBed } from '@angular/core/testing'
import { ToastService } from '../toast.service'
import { ToastrService, ToastrModule } from 'ngx-toastr'
import { NoopAnimationsModule } from '@angular/platform-browser/animations'

describe('ToastService', () => {
  const TIMEOUT_MOCK = 3000

  let toastrService: ToastrService
  let toastService: ToastService

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [ToastrModule.forRoot(), NoopAnimationsModule],
      providers: [ToastrService, ToastService],
    })
    toastrService = TestBed.get(ToastrService)
    toastService = TestBed.get(ToastService)
  })

  it('should be created', () => {
    expect(toastService).toBeTruthy()
  })

  it('should show success popup', () => {
    const mockObj = {
      timeOut: TIMEOUT_MOCK,
      positionClass: 'toast-bottom-right',
    }

    spyOn(toastrService, 'success')
    toastrService.success('', '', mockObj)

    expect(toastrService.success).toHaveBeenCalledTimes(1)
  })

  it('should show error popup', () => {
    const mockObj = {
      timeOut: TIMEOUT_MOCK,
      positionClass: 'toast-bottom-right',
    }

    spyOn(toastrService, 'error')
    toastrService.error('', '', mockObj)

    expect(toastrService.error).toHaveBeenCalledTimes(1)
  })
})
