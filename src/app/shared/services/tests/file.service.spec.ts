import { TestBed, async } from '@angular/core/testing'

import { FileService } from '../file.service'
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing'
import { environment } from '../../../../environments/environment'

describe('FileService', () => {
  let fileService: FileService
  let httpTestingController: HttpTestingController

  const url: string = environment.apiUrl

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FileService],
    })
    fileService = TestBed.get(FileService)
    httpTestingController = TestBed.get(HttpTestingController)
  })

  it('should be created', () => {
    const service: FileService = TestBed.get(FileService)
    expect(service).toBeTruthy()
  })

  it('should download file', () => {
    const paramName = 'fileName'

    fileService.downloadFile(paramName).subscribe((value) => {
      expect(value).toBeTruthy()
    })

    const req = httpTestingController.expectOne(
      (request) => request.url === `${url}/getPdfFile`
    )

    expect(req.request.method).toBe('GET')
    expect(req.request.params.get('name')).toBe(paramName)
    expect(req.request.headers.get('Accept')).toBe('application/pdf')
    expect(req.request.responseType).toBe('blob')
    req.flush(new Blob())
  })
})
