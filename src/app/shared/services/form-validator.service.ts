import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

export const validatorErrors = {
  required: 'Pole wymagane',
  minLength(minLength: string) {
    return `Pole musi mieć co najmniej ${minLength} znakow`;
  },
  email: 'Wymagany e-mail'
};

@Injectable({
  providedIn: 'root'
})
export class FormValidatorService {

  constructor() { }

  public validateField(control: AbstractControl): string {
    if (control.hasError('required')) {
      return validatorErrors.required;
    }
    if (control.hasError('minlength')) {
      return validatorErrors.minLength(control.getError('minlength').requiredLength);
    }
    if (control.hasError('email')) {
      return validatorErrors.email;
    }
  }

}
