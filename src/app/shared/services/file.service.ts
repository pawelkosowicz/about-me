import { Injectable } from '@angular/core'
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root',
})
export class FileService {
  private url: string = environment.apiUrl

  constructor(private httpClient: HttpClient) {}

  public downloadFile(name: string): Observable<any> {
    const params = new HttpParams().set('name', name)
    const headers = new HttpHeaders().set('Accept', 'application/pdf')

    return this.httpClient.get(`${this.url}/getPdfFile`, {
      headers,
      params,
      responseType: 'blob',
    })
  }
}
