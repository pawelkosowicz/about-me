export interface IPerson {
    name: string;
    email: string;
    adres: string;
    birthday: Date;
    nationality: string;
};