import { compose, RxwebValidators } from '@rxweb/reactive-form-validators';

export class ContactForm {

    @compose({validators: [RxwebValidators.required()]})
    name: string;

    @compose({validators: [RxwebValidators.required(), RxwebValidators.email()]})
    email: string;

    @compose({validators: [RxwebValidators.required()]})
    subject: string;

    @compose({validators: [RxwebValidators.required(), RxwebValidators.minLength({value: 10})]})
    message: string;
}
