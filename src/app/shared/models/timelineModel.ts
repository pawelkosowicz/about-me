export interface ITimelineModel {
    selected: boolean;
    date: Date;
}