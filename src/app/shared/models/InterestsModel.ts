export class Interest {

  public title: string;
  public imagePath: string;
  public image: string;
  public fromDate: Date;
  public toDate: Date;
  public description: string;

  constructor(title: string, imagePath: string, fromDate: Date, toDate: Date, description: string, image ? : string) {
    this.title = title;
    this.imagePath = imagePath;
    this.image = image;
    this.fromDate = fromDate;
    this.toDate = toDate;
    this.description = description;
  }

}

export class InterestsModel {
  public type: string;
  public interestList: Interest[];

  constructor(type: string, interestList: Interest[]) {
    this.type = type;
    this.interestList = interestList;
  }
}
