export interface IFeatures {
    feature: string;
    description: string;
}
