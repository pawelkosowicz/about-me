export interface IProjectLinksModel {
    demo: string;
    code: string;
}

export interface IProjectsModel {
    title: string;
    image: string;
    description: string;
    technologies: string;
    links: IProjectLinksModel;
}