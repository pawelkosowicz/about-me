export interface ISelectModel {
  key: string;
  value: string;
  obj?: Object;
}

export const interestsSelect: ISelectModel[] = [
  { key: "bieganie", value: "bieganie" },
  { key: "podróżowanie", value: "podróżowanie" }
];
