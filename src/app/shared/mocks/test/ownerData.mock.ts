import { IPerson } from '../../models/PersonModel';

export const personalDataMock: IPerson = {
    name: 'Jan Kowalski',
    email: 'test@wp.pl',
    adres: 'Warszawa, Polska',
    nationality: 'Polska',
    birthday: new Date('1998-08-08T00:00:00.000Z')
};
