import { IProjectsModel } from '../../models/ProjectsModel';

export const projectsMock: IProjectsModel[] = [
    {
        title: 'Strona Portfolio',
        image: '',
        description: 'Prosta strona napisana z wykorzystaniem technologii Angular',
        technologies: 'HTML | Sass | Typescript | Angular | Angular Material | Node.js | npm | Express',
        links: {
            demo: 'http://194.36.88.31/about-me',
            code: 'https://bitbucket.org/pawelkosowicz/about-me/'
        }
    },
    {
        title: 'Strona Dieta',
        image: '',
        description: 'Strona zwierajaca zestaw kalkulatorów obliczających zapotrzebowanie kaloryczne, wykorzystująca PWA',
        technologies: 'HTML | Sass | Angular | Bootstrap | Node.js | npm | PWA',
        links: {
            demo: 'https://pawelkosowicz.github.io/dieta/',
            code: 'https://bitbucket.org/pawelkosowicz/diet-pwa'
        }
    }
];