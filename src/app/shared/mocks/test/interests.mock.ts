import { InterestsModel } from '../../models/InterestsModel';

export const interestListMock: InterestsModel = {
    type: 'bieganie',
    interestList: [
        {
            title: 'Botaniczna Piątka – edycja wirtu@lna nocna',
            imagePath: 'bieganie/botaniczna_piatka_nocna_2020.jpg',
            image: '',
            fromDate: new Date('2020-05-20T00:00:00.000Z'),
            toDate: new Date('2020-05-20T00:00:00.000Z'),
            description: ''
        }
    ]
};
