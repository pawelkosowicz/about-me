import { IFeatures } from '../../models/Features';

export const ownerFeautresMock: IFeatures[] = [
    {
        feature: 'Komunikatywność',
        description: 'posiadam zdolność nawiązywania oraz utrzymywania realcji z innymi'
    },
    {
        feature: 'Uczciwość',
        description: 'zawsze staram się wywiązywać z obietnic oraz dotrzymywać danego słowa'
    },
    {
        feature: 'Sumienność',
        description: 'do wypełniania swoich obowiązków podchodzę skrupulatnie z dbałością o szczegóły'
    }
];
