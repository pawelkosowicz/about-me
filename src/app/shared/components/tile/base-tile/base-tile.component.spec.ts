import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseTileComponent } from './base-tile.component';

describe('BaseTileComponent', () => {
  let component: BaseTileComponent;
  let fixture: ComponentFixture<BaseTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
