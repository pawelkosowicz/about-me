import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.scss']
})
export class ServerErrorComponent implements OnInit {

  @Input()
  public message?: string;
  
  constructor() { 
    this.message = 'Błąd połączenia z serwerem';
  }

  ngOnInit() {

  }

}
