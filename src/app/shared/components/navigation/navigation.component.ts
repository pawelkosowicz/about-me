import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  private activeMenuElement: HTMLElement;

  @ViewChild('nav',{ static:false })
  private nav: ElementRef;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  public showNav(nav: HTMLElement) {
    nav.style.transform = `translateX(0%)`;
  }

  public hideNav(nav: HTMLElement) {
    nav.style.transform = `translateX(-100%)`;
  }

  public redirectToUrl(url: string, element: MouseEvent) {
    this.setActiveElement(element);
    this.hideNav(this.nav.nativeElement);
    this.router.navigate([`portfolio/${url}`]);
  }

  private setActiveElement(element: MouseEvent) {
    if (this.activeMenuElement) {
      this.activeMenuElement.classList.remove('active');
    }
    (element.target as HTMLElement).classList.add('active');
    this.activeMenuElement = element.target as HTMLElement;
  }

}
