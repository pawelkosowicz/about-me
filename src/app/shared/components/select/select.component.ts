import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  ViewChild,
  ElementRef,
} from '@angular/core'

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
})
export class SelectComponent implements OnInit, OnChanges {
  @Input('style')
  style?: string

  @Input('selectData')
  selectData: string[]

  // tslint:disable-next-line: no-input-rename
  @Input('defaultValueIndex')
  defaultValueIndex = 0

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onSearchEmmit: EventEmitter<string>

  @ViewChild('input', { static: true })
  public input: ElementRef

  public filteredData: string[]

  constructor() {
    this.onSearchEmmit = new EventEmitter()
    this.filteredData = []
  }

  ngOnInit() {}

  ngOnChanges(): void {
    if (
      this.selectData.length > 0 &&
      typeof this.defaultValueIndex !== 'undefined'
    ) {
      const initValue = this.selectData[this.defaultValueIndex]
      ;(this.input.nativeElement as HTMLInputElement).value = initValue
      this.onSearchEmmit.emit(initValue)
    }
  }

  public onKeyPress(event: any): void {
    if (event.charCode === 13 || event.code === 'Enter') {
      const value = event.target.value
      if (value.length > 0) {
        this.filteredData.splice(0)
        this.onSearchEmmit.emit(value)
      }
    }
  }

  public onInputChange(value: string): void {
    if (value.length === 0) {
      this.filteredData = [...this.selectData]
    } else if (value.length > 0) {
      this.filteredData = this.getFilterData(value)
    }
  }

  public onInputClick(value: string): void {
    if (value.length === 0) {
      this.filteredData = [...this.selectData]
    } else if (value.length > 0) {
      this.filteredData = this.getFilterData(value)
    }
  }

  public onSelectOption(value: string, input: HTMLInputElement): void {
    input.value = value
    this.filteredData.splice(0)
    this.onSearchEmmit.emit(value)
  }

  private getFilterData(value: string): string[] {
    return [...this.selectData]
    /* return this.selectData.filter((type: string) =>
      type.includes(value)      
    )*/
  }
}
