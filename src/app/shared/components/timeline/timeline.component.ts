import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  OnChanges,
} from '@angular/core'
import { ITimelineModel } from '../../models/TimelineModel'

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss'],
})
export class TimelineComponent implements OnInit, OnChanges {
  @Input('dates')
  public dates: Date[]

  public timelineDate: ITimelineModel[]
  public pickDate: Date

  @Output('dateChangeEmmiter')
  public dateChangeEmmiter: EventEmitter<Date>

  @Output('scrollEventEmmiter')
  public scrollEventEmmiter: EventEmitter<string>

  constructor() {
    this.timelineDate = []
    this.dateChangeEmmiter = new EventEmitter()
    this.scrollEventEmmiter = new EventEmitter()
    this.pickDate = new Date()
  }

  ngOnInit() {}

  ngOnChanges(): void {
    if (this.dates.length > 0) {
      this.setTimelineDate(this.pickDate)
    }
  }

  private setTimelineDate(pickDate: Date) {
    this.timelineDate = []
    const pickYear: string = pickDate.getFullYear().toString()
    this.timelineDate = this.dates.map((date: Date) => {
      const timelineYear: string = date.getFullYear().toString()
      if (timelineYear === pickYear) {
        return { selected: true, date }
      }
      return { selected: false, date }
    })
  }

  public onDateChange(date: Date) {
    this.pickDate = date
    this.setTimelineDate(date)
    this.dateChangeEmmiter.next(date)
  }

  public onScrollUp($event?: any) {
    this.scrollEventEmmiter.next('up')
  }

  public onScrollDown($event?: any) {
    this.scrollEventEmmiter.next('down')
  }
}
