import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TimelineComponent } from '../timeline.component';

@Component({
  selector: 'app-timeline-vertical',
  templateUrl: '../timeline.component.html',
  styleUrls: ['../timeline.component.scss', './timeline-vertical.component.scss']
})
export class TimelineVerticalComponent extends TimelineComponent implements OnInit  {
  
  constructor() {
    super();
  }

  ngOnInit() {
  }

}
