import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-cookie-law',
  templateUrl: './cookie-law.component.html',
  styleUrls: ['./cookie-law.component.scss'],
})
export class CookieLawComponent implements OnInit {
  public isCookieLawConfirmed: boolean
  private readonly STORAGE_NAME: string

  constructor() {
    this.isCookieLawConfirmed = false
    this.STORAGE_NAME = 'confirmedCookieLaw'
  }

  ngOnInit() {
    /* hide cookie modal for now */
    localStorage.setItem(this.STORAGE_NAME, 'true')

    const confirmedCookieLaw: string = localStorage.getItem(this.STORAGE_NAME)

    if (
      typeof confirmedCookieLaw !== 'undefined' &&
      Boolean(confirmedCookieLaw)
    ) {
      this.isCookieLawConfirmed = true
    }
  }

  public hideInformation(): void {
    localStorage.setItem(this.STORAGE_NAME, 'true')
    this.isCookieLawConfirmed = true
  }
}
