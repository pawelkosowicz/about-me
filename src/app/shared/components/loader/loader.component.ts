import { Component, OnInit, Input } from '@angular/core'

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
  @Input()
  public width = '20px'

  @Input()
  public height = '20px'

  constructor() {}

  ngOnInit() {}
}
