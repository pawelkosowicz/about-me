import {
  Directive,
  Output,
  EventEmitter,
  HostListener,
  ElementRef,
} from '@angular/core'
import { Subject, Observable, interval } from 'rxjs'
import { takeUntil, tap, filter } from 'rxjs/internal/operators'

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[holdable]',
})
export class HolderDirective {
  @Output() holdTime: EventEmitter<number> = new EventEmitter()

  state: Subject<string> = new Subject()

  cancel: Observable<string>

  constructor() {
    this.cancel = this.state.pipe(
      filter((v) => v === 'cancel'),
      tap((v) => {
        this.holdTime.emit(0)
      })
    )
  }

  @HostListener('mouseup')
  @HostListener('mouseleave')
  onExit() {
    this.state.next('cancel')
  }

  @HostListener('mousedown')
  onHold() {
    this.state.next('start')

    const n = 100
    interval(n)
      .pipe(
        takeUntil(this.cancel),
        tap((v) => {
          this.holdTime.emit(v * n)
        })
      )
      .subscribe()
  }
}
