export enum GAnalitycModuleEnum {
    ABOUTME = 'ABOUT-ME',
    CONTACT = 'CONTACT',
    INTEREST = 'INTEREST',
    PROJECT = 'PROJECT',
    SKILLS = 'SKILLS'
}

export enum GAnalitycEventEnum {
    CLICK_EVENT = 'CLICK',
    SHOW_VIEW = 'SHOW_VIEW',
    SEND_DATA = 'SEND_DATA'
}
