import { CommonModule } from '@angular/common'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { TileComponent } from './components/tile/tile.component'
import { MatFormFieldModule } from '@angular/material/form-field'
import {
  MatIconModule,
  MatInputModule,
  MatButtonModule,
  MatSelectModule,
} from '@angular/material'
import { HttpClientModule } from '@angular/common/http'
import { LoaderComponent } from './components/loader/loader.component'
import { CookieLawComponent } from './components/cookie-law/cookie-law.component'
import { SelectComponent } from './components/select/select.component'
import { TimelineComponent } from './components/timeline/timeline.component'
import { TimelineVerticalComponent } from './components/timeline/timeline-vertical/timeline-vertical.component'
import { BaseTileComponent } from './components/tile/base-tile/base-tile.component'
import { ServerErrorComponent } from './components/server-error/server-error.component'
import { HolderDirective } from './components/holder/holder.directive'
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators'
import { NgModule } from '@angular/core'

@NgModule({
  declarations: [
    TileComponent,
    LoaderComponent,
    CookieLawComponent,
    SelectComponent,
    TimelineComponent,
    TimelineVerticalComponent,
    BaseTileComponent,
    ServerErrorComponent,
    HolderDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    HttpClientModule,
  ],
  exports: [
    TileComponent,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatButtonModule,
    HttpClientModule,
    LoaderComponent,
    CookieLawComponent,
    SelectComponent,
    TimelineComponent,
    TimelineVerticalComponent,
    BaseTileComponent,
    ServerErrorComponent,
  ],
})
export class SharedModule {}
