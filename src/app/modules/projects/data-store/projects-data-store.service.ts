import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/internal/operators';
import { IProjectsModel } from '../../../shared/models/ProjectsModel';
import { ProjectsService } from '../services/projects.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectsDataStoreService {

  private allProjects$: Observable<IProjectsModel[]>;

  constructor(private projectsService: ProjectsService) {
  }

  public getAllProjects() {
    if(!this.allProjects$) {
      this.allProjects$ = this.projectsService.getAllProjects()
      .pipe(shareReplay());
    }

    return this.allProjects$;
  }
}
