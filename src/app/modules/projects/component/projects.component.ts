import { Component, OnInit } from '@angular/core';
import { GAnalitycModuleEnum, GAnalitycEventEnum } from '../../../shared/enums/gAnalityc.enum';
import { IProjectsModel } from '../../../shared/models/ProjectsModel';
import { GoogleAnalitycService } from '../../../shared/services/google-analityc.service';
import { ProjectsDataStoreService } from '../data-store/projects-data-store.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  public allProjects: IProjectsModel[];
  public isLoading: boolean;
  public showError: boolean;

  constructor(
    private projectsService: ProjectsDataStoreService,
    private gAnalityService: GoogleAnalitycService
  ) {
    this.isLoading = false;
    this.showError = false;
    this.allProjects = [];
  }

  ngOnInit() {
    this.getAllProjects();
    this.gAnalityService.sendEvent(GAnalitycModuleEnum.PROJECT, GAnalitycEventEnum.SHOW_VIEW, 'project');
  }

  public getAllProjects() {
    this.isLoading = true;
    this.showError = false;
    this.projectsService.getAllProjects().subscribe((projects: IProjectsModel[]) => {
      this.allProjects = projects;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
      this.showError = true;
    });
    this.gAnalityService.sendEvent(GAnalitycModuleEnum.PROJECT, GAnalitycEventEnum.SEND_DATA, 'getAllProjects');
  }

  public openNewPage(link: string) {
    this.gAnalityService.sendEvent(GAnalitycModuleEnum.PROJECT, GAnalitycEventEnum.CLICK_EVENT, link);
    window.open(link, '_blank');
  }

}
