import { TestBed } from '@angular/core/testing'

import { ProjectsService } from './projects.service'
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing'
import { environment } from '../../../../environments/environment'
import { IProjectsModel } from '../../../shared/models/ProjectsModel'
import { projectsMock } from '../../../shared/mocks/test/projects.mock'

describe('ProjectsService', () => {
  let projectService: ProjectsService
  let httpTestingController: HttpTestingController

  const url: string = environment.apiUrl

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProjectsService],
    })

    projectService = TestBed.get(ProjectsService)
    httpTestingController = TestBed.get(HttpTestingController)
  })

  it('should be created', () => {
    expect(TestBed.get(ProjectsService)).toBeTruthy()
  })

  it('should get all projects', () => {
    projectService
      .getAllProjects()
      .subscribe((projectsModel: IProjectsModel[]) => {
        expect(projectsModel).toBeTruthy()
        expect(projectsModel.length).toBe(2)
      })

    const req = httpTestingController.expectOne(`${url}/getAllProjects`)
    expect(req.request.method).toEqual('GET')

    req.flush(projectsMock)
  })

  it('shouldnt have any projects', () => {
    projectService
      .getAllProjects()
      .subscribe((projectsModel: IProjectsModel[]) => {
        expect(projectsModel).toBeTruthy()
        expect(projectsModel.length).toBe(0)
      })

    const req = httpTestingController.expectOne(`${url}/getAllProjects`)
    expect(req.request.method).toEqual('GET')

    req.flush([])
  })
})
