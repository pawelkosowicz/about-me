import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { IProjectsModel } from '../../../shared/models/ProjectsModel';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  private url: string = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  public getAllProjects(): Observable<IProjectsModel[]> {
    return this.httpClient.get<IProjectsModel[]>(`${this.url}/getAllProjects`);
  }

}
