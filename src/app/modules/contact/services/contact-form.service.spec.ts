import { TestBed } from '@angular/core/testing';

import { ContactFormService } from './contact-form.service';
import { environment } from '../../../../environments/environment';
import { ContactForm } from '../../../shared/models/ContactForm';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('ContactFormService', () => {

  let contactService: ContactFormService;
  let httpTestingController: HttpTestingController;

  const url: string = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ContactFormService]
    });
  });

  beforeEach(() => {
    contactService = TestBed.get(ContactFormService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('service should be create', () => {
     expect(contactService).toBeTruthy();
  });

  it('form should be send', () => {

    const contactForm: ContactForm = {
      email: 'test@test.com',
      message: 'Test message',
      name: 'Jan Kowalski',
      subject: 'Subject'
    };

    contactService.sendForm(contactForm)
    .subscribe((value) => {
      expect(value).toBeTruthy();
    });

    const req = httpTestingController.expectOne(`${url}/contactFormSend`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toBe(contactForm);
    req.flush({});
  });

});
