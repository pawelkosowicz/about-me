import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactForm } from '../../../shared/models/ContactForm';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactFormService {

  private url: string = environment.apiUrl;

  constructor(private httpClient: HttpClient) {
  }

  public sendForm(contactForm: ContactForm): Observable<any> {
    return this.httpClient.post(`${this.url}/contactFormSend`, contactForm);
  }

}
