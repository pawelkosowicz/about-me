import { Component, OnInit } from '@angular/core'
import { saveAs } from 'file-saver'
import { ContactForm } from '../../../shared/models/ContactForm'
import { FileService } from '../../../shared/services/file.service'
import { FormValidatorService } from '../../../shared/services/form-validator.service'
import { ContactFormService } from '../services/contact-form.service'
import { RxFormBuilder, IFormGroup } from '@rxweb/reactive-form-validators'
import { ToastService } from '../../../shared/services/toast.service'
import { GoogleAnalitycService } from '../../../shared/services/google-analityc.service'
import {
  GAnalitycModuleEnum,
  GAnalitycEventEnum,
} from '../../../shared/enums/gAnalityc.enum'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  public contactForm: IFormGroup<ContactForm>
  public isSendingEmail: boolean
  public showError: boolean

  constructor(
    private fb: RxFormBuilder,
    public formValidatorService: FormValidatorService,
    private contactFormService: ContactFormService,
    private toastService: ToastService,
    private fileService: FileService,
    private gAnalityService: GoogleAnalitycService
  ) {
    this.isSendingEmail = false
    this.showError = false
  }

  ngOnInit(): void {
    this.initForm()
    this.gAnalityService.sendEvent(
      GAnalitycModuleEnum.CONTACT,
      GAnalitycEventEnum.SHOW_VIEW,
      'contact'
    )
  }

  private initForm() {
    const contactForm = new ContactForm()
    this.contactForm = this.fb.formGroup(contactForm) as IFormGroup<ContactForm>
  }

  public resetForm() {
    this.contactForm.reset()
    Object.keys(this.contactForm.controls).forEach((key) => {
      this.contactForm.controls[key].setErrors(null)
    })
  }

  public sendForm(formValues: ContactForm) {
    this.isSendingEmail = true
    this.showError = false
    this.contactFormService.sendForm(formValues).subscribe(
      (response) => {
        this.isSendingEmail = false
        this.resetForm()
        this.toastService.showSuccesPopup('Wiadomość została wysłana', '')
      },
      (error: any) => {
        this.isSendingEmail = false
        this.showError = true
      }
    )
    this.gAnalityService.sendEvent(
      GAnalitycModuleEnum.CONTACT,
      GAnalitycEventEnum.SEND_DATA,
      'sendForm'
    )
  }

  public isFormFieldValid(formControlName: string): boolean {
    return this.contactForm.get(formControlName).invalid
  }

  public downloadCv(event: MouseEvent) {
    const fileName = 'pawel_kosowicz_cv'
    this.fileService.downloadFile(fileName).subscribe((file: Blob) => {
      saveAs(file, `${fileName}.pdf`)
    })
    event.preventDefault()
  }
}
