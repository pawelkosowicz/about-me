import { SharedModule } from '../../../shared/shared.module'
import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ContactComponent } from './contact.component'
import { RouterTestingModule } from '@angular/router/testing'
import { CommonModule } from '@angular/common'
import { ContactFormService } from '../services/contact-form.service'
import { NoopAnimationsModule } from '@angular/platform-browser/animations'
import { ContactForm } from '../../../shared/models/ContactForm'
import { IFormGroup, RxFormBuilder } from '@rxweb/reactive-form-validators'
import { of } from 'rxjs'
import { FileService } from '../../../shared/services/file.service'
import { GoogleAnalitycService } from '../../../shared/services/google-analityc.service'
import { ToastService } from '../../../shared/services/toast.service'

describe('ContactComponent', () => {
  let component: ContactComponent
  let fixture: ComponentFixture<ContactComponent>
  let contactFormService: ContactFormService

  const googleAnalitycSpy = jasmine.createSpyObj('GoogleAnalitycService', [
    'sendEvent',
  ])
  const toastServiceSpy = jasmine.createSpyObj('ToastService', [
    'showSuccesPopup',
    'showErrorPopup',
  ])

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactComponent],
      imports: [
        CommonModule,
        NoopAnimationsModule,
        RouterTestingModule,
        SharedModule,
      ],
      providers: [
        ContactFormService,
        RxFormBuilder,
        FileService,
        { provide: ToastService, useValue: toastServiceSpy },
        { provide: GoogleAnalitycService, useValue: googleAnalitycSpy },
      ],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactComponent)
    component = fixture.componentInstance
    contactFormService = TestBed.get(ContactFormService)
    fixture.detectChanges()
  })

  it('component should create', () => {
    expect(component).toBeTruthy()
  })

  it('click submit button should send form', () => {
    const contactForm = new ContactForm()
    contactForm.email = 'test@example.com'
    contactForm.message = 'Test message'
    contactForm.name = 'Test'
    contactForm.subject = 'Test subject'
    component.contactForm = TestBed.get(RxFormBuilder).formGroup(
      contactForm
    ) as IFormGroup<ContactForm>

    spyOn(component, 'sendForm').and.callThrough()
    spyOn(contactFormService, 'sendForm')
      .withArgs(contactForm)
      .and.returnValue(of({}))

    expect(component.contactForm.valid).toBeTruthy()

    component.sendForm(contactForm)

    expect(component.sendForm).toHaveBeenCalled()
    expect(component.sendForm).toHaveBeenCalledWith(contactForm)
    expect(contactFormService.sendForm).toHaveBeenCalled()
    expect(component.isSendingEmail).toBeFalsy()
  })

  it('should check set form field to invalid', () => {
    const result = component.isFormFieldValid('email')
    expect(result).toBeTruthy()
  })

  it('should download CV', () => {
    const fileService = TestBed.get(FileService)
    const spy = spyOn(fileService, 'downloadFile').and.returnValue(
      of(new Blob())
    )
    component.downloadCv(new MouseEvent('click'))
    expect(spy).toHaveBeenCalled()
  })
})
