import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { IFeatures } from '../../../shared/models/Features';
import { IPerson } from '../../../shared/models/PersonModel';

@Injectable({
  providedIn: 'root'
})
export class AboutMeService {

  private url: string = environment.apiUrl;

  constructor(private httpClient: HttpClient) {  }

  public getOwnerData(): Observable<IPerson> {
    return this.httpClient.get<IPerson>(`${this.url}/getOwnerData`);
  }

  public getOwnerFeatures(): Observable<Array<IFeatures>> {
    return this.httpClient.get<Array<IFeatures>>(`${this.url}/getOwnerFeatures`);
  }

  public getActually(): Observable<string> {
    return this.httpClient.get<string>(`${this.url}/getActually`);
  }

}
