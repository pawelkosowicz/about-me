import {AboutMeService} from './about-me.service';
import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../../environments/environment';
import { IPerson } from '../../../shared/models/PersonModel';
import { IFeatures } from '../../../shared/models/Features';
import { personalDataMock } from '../../../shared/mocks/test/ownerData.mock';
import { ownerFeautresMock } from '../../../shared/mocks/test/ownerFeatures.mock';
import { actuallyMock } from '../../../shared/mocks/test/actually.mock';

describe('AboutMeService', () => {

  let aboutMeService: AboutMeService;
  let httpTestingController: HttpTestingController;

  const url: string = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AboutMeService
      ]
    });
  });

  beforeEach(() => {
    aboutMeService = TestBed.get(AboutMeService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('service should create', () => {
    expect(aboutMeService).toBeTruthy();
  });

  it('should get owner data', () => {

    aboutMeService.getOwnerData()
    .subscribe((value: IPerson) => {
      expect(value).toBeTruthy();
    });

    const req = httpTestingController.expectOne(`${url}/getOwnerData`);
    expect(req.request.method).toBe('GET');

    req.flush(personalDataMock);
  });

  it('should get owner features', () => {

    aboutMeService.getOwnerFeatures()
    .subscribe((value: Array<IFeatures>) => {
      expect(value).toBeTruthy();
      expect(Array.isArray(value)).toBeTruthy();
    });

    const req = httpTestingController.expectOne(`${url}/getOwnerFeatures`);
    expect(req.request.method).toBe('GET');

    req.flush(ownerFeautresMock);
  });

  it('should get actually information', () => {

    aboutMeService.getActually()
    .subscribe((value: string) => {
      expect(value).toBeTruthy();
    });

    const req = httpTestingController.expectOne(`${url}/getActually`);
    expect(req.request.method).toBe('GET');

    req.flush(actuallyMock);
  });

});
