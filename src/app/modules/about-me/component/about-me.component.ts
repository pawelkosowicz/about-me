import {switchMap} from 'rxjs/internal/operators';
import { Component, OnInit } from '@angular/core';
import { IFeatures } from '../../../shared/models/Features';
import { IPerson } from '../../../shared/models/PersonModel';
import { AboutMeDataStoreService } from '../data-store/about-me-data-store.service';
import { GoogleAnalitycService } from '../../../shared/services/google-analityc.service';
import { GAnalitycEventEnum, GAnalitycModuleEnum } from '../../../shared/enums/gAnalityc.enum';

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss']
})
export class AboutMeComponent implements OnInit {

  public features: Array<IFeatures>;
  public person: IPerson;
  public description: string;
  public isLoading: boolean;
  public showError: boolean;

  constructor(
    private aboutMeStoreService: AboutMeDataStoreService,
    private gAnalityService: GoogleAnalitycService
    ) {
    this.features = [];
    this.person = undefined;
    this.description = undefined;
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.showError = false;
    this.getOwnerFeautures();
    this.gAnalityService.sendEvent(GAnalitycModuleEnum.ABOUTME, GAnalitycEventEnum.SHOW_VIEW, 'about-me');
  }

  private getOwnerFeautures() {
    this.aboutMeStoreService.getOwnerFeatures().pipe(
      switchMap((feautres: Array<IFeatures>) => {
        this.features = feautres;
        return this.aboutMeStoreService.getOwnerData();
      }),
      switchMap((person: IPerson) => {
        this.person = person;
        return this.aboutMeStoreService.getActually();
      })
    ).subscribe((description: string) => {
      this.description = description;
      this.isLoading = false;
    }, (error) => {
      this.isLoading = false;
      this.showError = true;
    });
  }

}
