import { SharedModule } from '../../../shared/shared.module'
import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AboutMeComponent } from './about-me.component'
import { RouterTestingModule } from '@angular/router/testing'
import { CommonModule } from '@angular/common'
import { GoogleAnalitycService } from '../../../shared/services/google-analityc.service'
import { AboutMeDataStoreService } from '../data-store/about-me-data-store.service'

const a = spyOn
describe('AboutMeComponent', () => {
  let component: AboutMeComponent
  let fixture: ComponentFixture<AboutMeComponent>

  const googleAnalitycSpy = jasmine.createSpyObj('GoogleAnalitycService', [
    'sendEvent',
  ])

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutMeComponent],
      imports: [CommonModule, RouterTestingModule, SharedModule],
      providers: [
        AboutMeDataStoreService,
        { provide: GoogleAnalitycService, useValue: googleAnalitycSpy },
      ],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutMeComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('component should create', () => {
    expect(component).toBeTruthy()
  })
})
