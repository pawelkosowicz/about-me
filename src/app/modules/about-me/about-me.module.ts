import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AboutMeComponent } from './component/about-me.component';

const routes: Routes = [
  { path: '', component: AboutMeComponent }
];

@NgModule({
  declarations: [AboutMeComponent],
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
  exports: [RouterModule]
})
export class AboutMeModule {}
