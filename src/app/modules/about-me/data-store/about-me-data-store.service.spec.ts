import {AboutMeService} from '../services/about-me.service';
import { async, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';

import { AboutMeDataStoreService } from './about-me-data-store.service';
import { Observable, of, ReplaySubject } from 'rxjs';
import { personalDataMock } from '../../../shared/mocks/test/ownerData.mock';
import { IPerson } from '../../../shared/models/PersonModel';
import { ownerFeautresMock } from '../../../shared/mocks/test/ownerFeatures.mock';
import { IFeatures } from '../../../shared/models/Features';
import { actuallyMock } from '../../../shared/mocks/test/actually.mock';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AboutMeDataStoreService', () => {
  let aboutMeDSService: AboutMeDataStoreService;
  let aboutMeService: AboutMeService;

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers : [
        AboutMeDataStoreService,
        AboutMeService
      ]})
  );

  beforeEach(inject([AboutMeDataStoreService, AboutMeService], (service: AboutMeDataStoreService, aboutService: AboutMeService) => {
    aboutMeDSService = service;
    aboutMeService = aboutService;
  }));

  it('service should create', () => {
    expect(aboutMeDSService).toBeTruthy();
  });

  it('should get owner data', async(() => {
    aboutMeDSService['ownerData$'] = undefined;
    const spy = spyOn(aboutMeDSService, 'getOwnerData').and.returnValue(of(personalDataMock));

    aboutMeDSService.getOwnerData().subscribe((v: IPerson) => {
      expect(v).toEqual(personalDataMock);
    });

    expect(spy).toHaveBeenCalled();
  }));

  it('should get owner features', async(() => {
    aboutMeDSService['ownerFeatures$'] = undefined;
    const spy = spyOn(aboutMeDSService, 'getOwnerFeatures').and.returnValue(of(ownerFeautresMock));

    aboutMeDSService.getOwnerFeatures().subscribe((v: Array<IFeatures>) => {
      expect(v.length).toEqual(ownerFeautresMock.length);
    });
    expect(spy).toHaveBeenCalled();
  }));

  it('should get sctually information', async(() => {
    aboutMeDSService['actually$'] = undefined;
    const spy = spyOn(aboutMeDSService, 'getActually').and.returnValue(of(actuallyMock));

    aboutMeDSService.getActually().subscribe((v: string) => {
      expect(v).toEqual(actuallyMock);
    });
    expect(spy).toHaveBeenCalled();
  }));

  it('should clear cache', () => {
    aboutMeDSService.clearCache();
    expect(aboutMeDSService['actually$']).toEqual(null);
    expect(aboutMeDSService['ownerData$']).toEqual(null);
    expect(aboutMeDSService['ownerFeatures$']).toEqual(null);
  });



});
