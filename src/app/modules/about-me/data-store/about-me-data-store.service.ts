import { Injectable } from '@angular/core';
import { AboutMeService } from '../services/about-me.service';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/internal/operators';
import { IFeatures } from '../../../shared/models/Features';
import { IPerson } from '../../../shared/models/PersonModel';

@Injectable({
  providedIn: 'root'
})
export class AboutMeDataStoreService {

  private ownerData$: Observable<IPerson>;
  private ownerFeatures$: Observable<Array<IFeatures>>;
  private actually$: Observable<string>;

  constructor(private aboutMeService: AboutMeService) {
  }

  public getOwnerData(): Observable<IPerson> {

    if (!this.ownerData$) {
      this.ownerData$ = this.aboutMeService.getOwnerData()
      .pipe(shareReplay());
    }

    return this.ownerData$;
  }

  public getOwnerFeatures(): Observable<Array<IFeatures>> {

    if (!this.ownerFeatures$) {
      this.ownerFeatures$ = this.aboutMeService.getOwnerFeatures()
      .pipe(shareReplay());
    }

    return this.ownerFeatures$;
  }

  public getActually(): Observable<string> {

    if (!this.actually$) {
      this.actually$ = this.aboutMeService.getActually()
      .pipe(shareReplay());
    }

    return this.actually$;
  }

  public clearCache() {
    this.actually$ = null;
    this.ownerData$ = null;
    this.ownerFeatures$ = null;
  }
}
