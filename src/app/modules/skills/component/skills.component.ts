import { Component, OnInit } from '@angular/core';
import { GAnalitycModuleEnum, GAnalitycEventEnum } from '../../../shared/enums/gAnalityc.enum';
import { GoogleAnalitycService } from '../../../shared/services/google-analityc.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  constructor(private gAnalityService: GoogleAnalitycService) { }

  ngOnInit() {
    this.gAnalityService.sendEvent(GAnalitycModuleEnum.SKILLS, GAnalitycEventEnum.SHOW_VIEW, 'skills');
  }

}
