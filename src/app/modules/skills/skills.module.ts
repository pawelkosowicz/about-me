import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { SharedModule } from '../../shared/shared.module'
import { SkillsComponent } from './component/skills.component'

const routes: Routes = [{ path: '', component: SkillsComponent }]

@NgModule({
  declarations: [SkillsComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SkillsModule {}
