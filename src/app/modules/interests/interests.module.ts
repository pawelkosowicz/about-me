import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InterestsComponent } from './component/interests.component';

const routes: Routes = [
  { path: '', component: InterestsComponent }
];

@NgModule({
  declarations: [InterestsComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    InterestsComponent,
    RouterModule
  ]
})
export class InterestsModule { }
