import { TestBed } from '@angular/core/testing'

import { InterestsService } from './interests.service'
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing'
import { InterestsModel } from '../../../shared/models/InterestsModel'
import { environment } from '../../../../environments/environment'
import { interestListMock } from '../../../shared/mocks/test/interests.mock'

describe('InterestsService', () => {
  let interestService: InterestsService
  let httpTestingController: HttpTestingController

  const url: string = environment.apiUrl

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [InterestsService],
    })

    interestService = TestBed.get(InterestsService)
    httpTestingController = TestBed.get(HttpTestingController)
  })

  it('should be created', () => {
    const service: InterestsService = TestBed.get(InterestsService)
    expect(service).toBeTruthy()
  })

  it('should return all interest type', () => {
    interestService.getInterestTypes().subscribe((value: string[]) => {
      expect(value).toBeTruthy()
      expect(value.length).toBe(2)
    })

    const req = httpTestingController.expectOne(`${url}/getAllInterestsType`)

    expect(req.request.method).toEqual('GET')

    req.flush(['bieganie', 'podróżowanie'])
  })

  it('should get interest list by type', () => {
    interestService
      .getInterest('bieganie', new Date(2020))
      .subscribe((interestModel: InterestsModel) => {
        expect(interestModel).toBeTruthy()
        expect(interestModel.type).toEqual('bieganie')
        expect(interestModel.interestList.length).toBe(1)
      })

    const req = httpTestingController.expectOne(
      (request) => request.url === `${url}/getInterest/`
    )

    expect(req.request.method).toEqual('GET')
    expect(req.request.params.get('type')).toEqual('bieganie')

    req.flush(interestListMock)
  })
})
