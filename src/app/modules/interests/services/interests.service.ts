import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { InterestsModel } from '../../../shared/models/InterestsModel';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InterestsService {

  private url: string = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  public getInterestList(): Observable<InterestsModel[]> {
    return this.httpClient.get<InterestsModel[]>(`${this.url}/getAllInterests`);
  }

  public getInterest(type: string, date: Date): Observable<InterestsModel> {
    const params = new HttpParams()
    .set('type', type)
    .set('year', date.getFullYear().toString());

    return this.httpClient.get<InterestsModel>(`${this.url}/getInterest/`, {params});
  }

  public getInterestTypes(): Observable<string[]> {
    return this.httpClient.get<string[]>(`${this.url}/getAllInterestsType`);
  }


}
