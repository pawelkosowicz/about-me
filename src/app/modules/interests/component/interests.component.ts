import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  HostListener,
  AfterViewChecked,
  ChangeDetectorRef,
} from '@angular/core'
import {
  GAnalitycEventEnum,
  GAnalitycModuleEnum,
} from '../../../shared/enums/gAnalityc.enum'
import { InterestsModel } from '../../../shared/models/InterestsModel'
import { GoogleAnalitycService } from '../../../shared/services/google-analityc.service'
import { InterestsService } from '../services/interests.service'

@Component({
  selector: 'app-interests',
  templateUrl: './interests.component.html',
  styleUrls: ['./interests.component.scss'],
})
export class InterestsComponent implements OnInit, AfterViewChecked {
  @ViewChild('contentList', { static: false })
  public contentList: ElementRef

  public interest: InterestsModel
  public interestTypes: string[]
  public isLoadingList: boolean
  public showError: boolean
  public pickDate: Date
  public timelineDates: Date[]

  constructor(
    private interestService: InterestsService,
    public changeDatectorRef: ChangeDetectorRef,
    private gAnalityService: GoogleAnalitycService
  ) {
    this.interestTypes = []
    this.isLoadingList = true
    this.showError = false
    this.pickDate = new Date()
    this.timelineDates = this.getTimelineDates()
  }

  @HostListener('window:resize', [])
  public onResize() {
    this.setListHeight()
  }

  ngOnInit() {
    this.getInterestTypes()
    this.gAnalityService.sendEvent(
      GAnalitycModuleEnum.INTEREST,
      GAnalitycEventEnum.SHOW_VIEW,
      'interest'
    )
  }
  ngAfterViewChecked(): void {
    this.setListHeight()
  }

  private getTimelineDates(): Date[] {
    const amountDates: number = new Date().getFullYear() - 2017
    const timelineDates = new Array(amountDates)
    return timelineDates.fill(new Date()).map((v: Date, index: number) => {
      const year = (v.getFullYear() - index).toString()
      return new Date(year)
    })
  }

  private getInterestTypes() {
    this.isLoadingList = true
    this.showError = false

    this.interestService.getInterestTypes().subscribe(
      (types) => {
        this.interestTypes.splice(0)
        this.interestTypes = types
        this.isLoadingList = false
      },
      () => {
        this.showError = true
      }
    )
  }

  private setListHeight(): void {
    if (
      typeof this.contentList !== 'undefined' &&
      typeof this.interest !== 'undefined'
    ) {
      const listElements = this.contentList.nativeElement as HTMLElement
      const children: Element[] = Array.from(listElements.children)

      const childrenCount: number = children.length > 2 ? 2 : children.length
      let listHeight = 0

      children.forEach((child: Element, index: number) => {
        if (index < childrenCount) {
          listHeight += child.clientHeight
        }
      })
      listElements.style.height = `${listHeight}px`
    } else if (typeof this.contentList !== 'undefined') {
      const listElements = this.contentList.nativeElement as HTMLElement
      listElements.style.height = `0px`
    }
  }

  private clearList() {
    this.interest = undefined
  }

  private getInterest(interestType: string, date: Date) {
    this.isLoadingList = true
    this.showError = false
    this.clearList()
    this.interestService.getInterest(interestType, date).subscribe(
      (interest: InterestsModel) => {
        this.isLoadingList = false
        this.interest = interest
        this.changeDatectorRef.detectChanges()
      },
      () => {
        this.showError = true
      }
    )
    this.gAnalityService.sendEvent(
      GAnalitycModuleEnum.INTEREST,
      GAnalitycEventEnum.SEND_DATA,
      'getInterest'
    )
  }

  public onSearchEvent(interestType: string) {
    this.getInterest(interestType, this.pickDate)
  }

  public onDateChange(date: Date): void {
    this.pickDate = date
    this.getInterest(this.interest.type, date)
  }

  public scrollList(direction: string) {
    const listElements = this.contentList.nativeElement as HTMLElement

    switch (direction) {
      case 'up':
        listElements.scrollBy(0, -100)
        break
      case 'down':
        listElements.scrollBy(0, 100)
        break
    }
  }
}
