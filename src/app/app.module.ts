import {BrowserAnimationsModule} from '@angular/platform-browser/animations/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './shared/components/navigation/navigation.component';
import { AboutMeModule } from './modules/about-me/about-me.module';
import { ContactModule } from './modules/contact/contact.module';
import { SkillsModule } from './modules/skills/skills.module';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from './shared/shared.module';
import { InterestsModule } from './modules/interests/interests.module';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AboutMeModule,
    ContactModule,
    SkillsModule,
    InterestsModule,
    ToastrModule.forRoot(),
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
