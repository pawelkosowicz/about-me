const PersonModel = require('../models/PersonModel');

const personMock = new PersonModel(
    "Pawel Kosowicz",
    "pawelkosowicz@wp.pl",
    "Warszawa, Polska",
    "Polska",
    new Date('1992-08-21')
)

module.exports = personMock;