const Interst = require("../models/InterestsModel").Interest;
const InterestsModel = require("../models/InterestsModel").InterestsModel;

const interstsList = [
  new InterestsModel("bieganie", [
    new Interst(
      "Botaniczna Piątka – edycja wirtu@lna nocna",
      "bieganie/botaniczna_piatka_nocna_2020.jpg",
      new Date("2020-06-05"),
      new Date("2020-06-05"),
      ""
    ),
    new Interst(
      "Botaniczna Piątka – edycja wirtu@lna wiosenna",
      "bieganie/bieg_pszczol_2020.jpg",
      new Date("2020-05-20"),
      new Date("2020-05-20"),
      ""
    ),
    new Interst(
      "31. Bieg Niepodległości Warszawa",
      "bieganie/bieg_niepodleglosci_2019.jpg",
      new Date("2019-11-11"),
      new Date("2019-11-11"),
      ""
    ),
    new Interst(
      "29. Bieg Powstania Warszawskiego",
      "bieganie/bieg_powstania_2019.jpg",
      new Date("2019-07-27"),
      new Date("2019-07-27"),
      ""
    ),  
    new Interst(
      "II Mistrzostwa Polski w Biegu na Skocznię",
      "bieganie/wisla_2019.jpg",
      new Date("2019-06-01"),
      new Date("2019-06-01"),
      ""
    ),  
    new Interst(
      "Bieg 80-lecia Stali Mielec",
      "bieganie/lotnisko_mielec_2019.jpg",
      new Date("2019-04-14"),
      new Date("2019-04-14"),
      ""
    ),  
    new Interst(
      "12. PKO Półmaraton Rzeszowski",
      "bieganie/polmaraton_rzeszow_2019.jpg",
      new Date("2019-04-7"),
      new Date("2019-04-7"),
      ""
    ), 
    new Interst(
      "3 mielecka dycha",
      "bieganie/mielecka_dycha_2019.jpg",
      new Date("2019-03-30"),
      new Date("2019-03-30"),
      ""
    ),
    new Interst(
      "Charytatywny bieg na hospicjum",
      "bieganie/hospicjum_2019.jpg",
      new Date("2019-03-24"),
      new Date("2019-03-24"),
      ""
    ),
    new Interst(
      "2. Bieg policz się z cukrzycą | Mielec",
      "bieganie/wosp_2019.jpg",
      new Date("2019-01-13"),
      new Date("2019-01-13"),
      ""
    ),
    new Interst(
      "6. PKO Bieg Niepodległości Rzeszow",
      "bieganie/bieg_niepodleglosci_2018.jpg",
      new Date("2018-11-11"),
      new Date("2018-11-11"),
      ""
    ),
    new Interst(
      "4 bieg o puchar pratt & whitney",
      "bieganie/pratt_whitney_2018.jpg",
      new Date("2018-10-06"),
      new Date("2018-10-06"),
      ""
    ),
    new Interst(
      "Rzeszów Business Run",
      "bieganie/rzeszow_business_run_2018.jpg",
      new Date("2018-09-02"),
      new Date("2018-09-08"),
      ""
    ),
    new Interst(
      "Bieg Żółwia IV Bieg Grodziski o puchar Wójta Gminy Grodzisko Dolne",
      "bieganie/bieg_zolwia_2018.jpg",
      new Date("2018-07-07"),
      new Date("2018-07-07"),
      ""
    ),
    new Interst(
      "3. Sparing 10 km",
      "bieganie/sparing_2018.jpg",
      new Date("2018-06-02"),
      new Date("2018-06-02"),
      ""
    ),
    new Interst(
      "Skyway Run",
      "bieganie/skyway_run_2018.jpg",
      new Date("2018-05-12"),
      new Date("2018-05-13"),
      ""
    ),
    new Interst(
      "2 mielecka dycha",
      "bieganie/mielecka_dycha_2018.jpg",
      new Date("2018-04-28"),
      new Date("2018-04-28"),
      ""
    )   
  ]),
  new InterestsModel("podróżowanie", [
    new Interst(
      "Beskid Sądecki",
      "podrozowanie/beskidy_2020.jpg",
      new Date("2020-08-17"),
      new Date("2020-08-20"),
      `<p><strong>Jaworzyna Krynicka</strong> (1114 m n.p.m.) - szczyt w Beskidzie Sądeckim, znajdujący się w Krynicy-Zdroju, na obszarze gminy Muszyna. Jest najwyższym szczytem w Paśmie Jaworzyny</p>
       <p><strong>Muszyna</strong> - posiada status miejscowości uzdrowiskowej z licznymi odwiertami i rozlewniami wód mineralnych. Znajdują się tam pijalnie wody mineralnej, sanatoria uzdrowiskowe, baseny oraz parki.</p>
       <p><strong>Krynica-Zdrój</strong> - położona jest w Beskidzie Sądeckim na wysokości 560-620 m n.p.m. w malowniczych dolinach Kryniczanki i jej dopływów, Krynicę-Zdrój otaczają wzgórza Góry Parkowej i pasmo Jaworzyny Krynickiej.</p>`
    ),
    new Interst(
      "Krosno",
      "podrozowanie/krosno_2020.jpg",
      new Date("2020-06-27"),
      new Date("2020-06-27"),
      `<p><strong>Muzeum Przemysłu Naftowego i Gazowniczego</strong> - kopalnia ropy naftowej w Bóbrce, obecnie Muzeum Przemysłu Naftowego i Gazowniczego im. Ignacego Łukasiewicza – otwarta w 1854 roku i nadal działająca, kopalnia ropy naftowej w Bóbrce w Gminie Chorkówka, koło Krosna.</p>
       <p><strong>Centrum Dziedzictwa Szkła</strong> - pierwszy obiekt turystyczno-kulturalny w Polsce, który prezentuje tematykę hutnictwa szkła oraz szeroko pojętej szklanej twórczości w interaktywny sposób.</p>`
    ),
    new Interst(
      "Bieszczady",
      "podrozowanie/bieszczady_2020.jpg",
      new Date("2020-06-19"),
      new Date("2020-06-21"),
      `<p><strong>Jawornik</strong> (1047 m n.p.m.) – całkowicie zalesiony szczyt w Bieszczadach Zachodnich, położony na północny wschód od Połoniny Wetlińskiej, w bocznym grzbiecie opadającym z Hasiakowej Skały</p>
      <p><strong>Przełęcz Orłowicza</strong> (1075 m n.p.m.)– przełęcz w Bieszczadach Zachodnich, w masywie Połoniny Wetlińskiej. Jest to szerokie siodło oddzielające Smerek od Szarego Berda w masywie Połoniny Wetlińskiej. W rzeczywistości są to dwa obniżenia rozdzielone niewielkim garbem.</p>`
    ),
    new Interst(
      "Prządki",
      "podrozowanie/przadki_2020.jpg",
      new Date("2020-05-16"),
      new Date("2020-05-16"),
      `Rezerwat obejmuje grupę ostańców skalnych, o wysokości do ponad 20 m, zbudowanych z gruboziarnistego piaskowca ciężkowickiego, który pod wpływem erozji przybrał oryginalne kształty. ... Prządki mają status rezerwatu od 1957 roku i są jedynym rezerwatem skalnym w południowo-wschodniej Polsce.
      `
    ),
    new Interst(
      "Ciężkowice",
      "podrozowanie/ciezkowice_2020.jpg",
      new Date("2020-05-10"),
      new Date("2020-05-10"),
      "Jednym z miejsc w Małopolsce, które szczególnie miło zwiedzić, jest rezerwat przyrody nieożywionej w Ciężkowicach – Skamieniałe Miasto. Skały posiadają swoje nazwy i związane z nimi historie. Jest Grunwald, Czarownica, Ratusz, Piramidy, Skałka z krzyżem i inne. Materiał pobrany z bloga mojamalopolska.pl, prezentującego ciekawe miejsca w Małopolsce i różne ciekawostki dotyczące tego regionu."
    ),

    new Interst(
      "Poznań",
      "podrozowanie/poznan_2020.jpg",
      new Date("2020-03-07"),
      new Date("2020-03-07"),
      "Położony jest nad rzekami Wartą i Cybiną, na Pojezierzu Wielkopolskim. Jest jednym z najstarszych i największych miast w Polsce oraz stolicą Wielkopolski i województwa wielkopolskiego. W jego obrębie znajduje się duży węzeł drogowy i kolejowy oraz międzynarodowy port lotniczy Ławica."
    ),
    new Interst(
      "Wrocław",
      "podrozowanie/wroclaw_2020.jpg",
      new Date("2020-02-28"),
      new Date("2020-03-01"),
      "Stolica Dolnego Śląska, jest jednym z najstarszych i najpiękniejszych miast w Polsce. Położony u podnóża Sudetów, nad rzeką Odrą , poprzecinany jej licznymi dopływami i kanałami, jest wyjątkowym miastem 12 wysp i ponad stu mostów. Bogata i burzliwa historia miasta wpisana jest w jego mury."
    ),
    new Interst(
      "Wrocław",
      "podrozowanie/wroclaw_2019.jpg",
      new Date("2019-11-04"),
      new Date("2019-11-05"),
      "Stolica Dolnego Śląska, jest jednym z najstarszych i najpiękniejszych miast w Polsce. Położony u podnóża Sudetów, nad rzeką Odrą , poprzecinany jej licznymi dopływami i kanałami, jest wyjątkowym miastem 12 wysp i ponad stu mostów. Bogata i burzliwa historia miasta wpisana jest w jego mury."
    ),
    new Interst(
      "Ukraina",
      "podrozowanie/odessa_2019.jpg",
      new Date("2019-09-24"),
      new Date("2019-10-03"),
      `<p><strong>Lwów</strong> - największe miasto zachodniej Ukrainy i jedno z centrów kulturalnych kraju. Lwowska Starówka została wpisana na Listę Światowego Dziedzictwa UNESCO.</p><br>
      <p><strong>Odessa</strong> - Trzecie co do wielkości miasto Ukrainy. Położone na Krymie miasto, gdzie liczba słonecznych dni przekracza 290, 
      a zima jest naprawdę krótka, urzeka zarówno piękną przyrodą, jak i mnóstwem ciekawych miejsc, które zachwycą każdego podróżnika.</p><br>
      <p><strong>Akkerman</strong> - Białogród nad Dniestrem to niewielkie miasto w południowej części Ukrainy, położone na Nizinie Czarnomorskiej, słynące z potężnej twierdzy Akerman, górującej nad Limanem Dniestru.</p>`
    ),
    new Interst(
      "Janów Lubelski",
      "podrozowanie/janow_lubelski_2019.jpg",
      new Date("2019-08-31"),
      new Date("2019-08-31"),
      `Usytuowany jest we wschodniej Polsce, w południowo-zachodniej części województwa lubelskiego w powiecie janowskim. Miasto jest ważnym węzłem komunikacyjnym, leżącym na skrzyżowaniu dróg wiodących do granic Polski - z Ukrainą w Zosinie (130 km) i Hrebennem (120 km) oraz ze Słowacją w Barwinku (200 km).`
    ),
    new Interst(
      "Krzyżtopór",
      "podrozowanie/krzyztopor_2019.jpg",
      new Date("2019-08-03"),
      new Date("2019-08-03"),
      `Ruiny powstałej w latach 1627–1644 rezydencji pałacowej otoczonej fortyfikacjami bastionowymi położonej w miejscowości Ujazd w województwie świętokrzyskim. Była to największa budowla pałacowa w Europie przed powstaniem Wersalu. Od 2018 ma status pomnika historii.`
    ),
    new Interst(
      "Sandomierz",
      "podrozowanie/sandomierz_2019.jpg",
      new Date("2019-06-03"),
      new Date("2019-06-03"),
      `Miasto o tysiącletniej historii, które leży nad Wisłą na krawędzi Wyżyny Sandomierskiej, na malowniczych wzgórzach poprzecinanych lessowymi wąwozami, w otoczeniu soczystej zieleni. Sandomierz to żywy świadek historii i kultury naszego Państwa i Narodu.`
    ),
    new Interst(
      "Wisła",
      "podrozowanie/wisla_2019.jpg",
      new Date("2019-06-01"),
      new Date("2019-06-01"),
      `Perła Beskidu Śląskiego. Centrum miasta znajduje się na wysokości 430 metrów n.p.m., a najwyższy punkt - 1220 m n.p.m. jest zlokalizowany na szczycie Baraniej Góry, na której zboczach swoje źródła ma Wisła.`
    ),
    new Interst(
      "Katowice",
      "podrozowanie/katowice_2019.jpg",
      new Date("2019-03-01"),
      new Date("2019-03-03"),
      `to największe miasto w województwie śląskim, w którym żyją ponad 303 tysiące osób. Jest on jednym z głównych ośrodków Górnośląskiego Okręgu Przemysłowego i siedzibą władz wojewódzkich. Początki miasta sięgają wieku XVI, kiedy to był on ośrodkiem kuźniczym i rolniczym.`
    ),
    new Interst(
      "Sandomierz",
      "podrozowanie/sandomierz_2018.jpg",
      new Date("2018-09-16"),
      new Date("2018-09-16"),
      `Miasto o tysiącletniej historii, które leży nad Wisłą na krawędzi Wyżyny Sandomierskiej, na malowniczych wzgórzach poprzecinanych lessowymi wąwozami, w otoczeniu soczystej zieleni. Sandomierz to żywy świadek historii i kultury naszego Państwa i Narodu.`
    ),
    new Interst(
      "Ukraina - Mołdawia",
      "podrozowanie/ukraina_2018.jpg",
      new Date("2018-08-11"),
      new Date("2018-08-19"),
      `<p><strong>Kijów</strong> - jedno z najstarszych miast Europy i jedna z kolebek kultury Rusi przez wielu nazywany "matką miast". Założony na początku V wieku, na szlaku łączącym Bizancjum ze Skandynawią, cztery stulecia później stał się miejscem, w którym Ruś przyjęła chrzest.</p><br>
      <p><strong>Odessa</strong> - trzecie co do wielkości miasto Ukrainy. Położone na Krymie miasto, gdzie liczba słonecznych dni przekracza 290, 
      a zima jest naprawdę krótka, urzeka zarówno piękną przyrodą, jak i mnóstwem ciekawych miejsc, które zachwycą każdego podróżnika.</p><br>
      <p><strong>Kiszyniów</strong> - miasto, gdzie naprawdę nic nie ma... Na pobieżne zwiedzenie miasta wystarczy jeden dzień. Kiszyniów nie do końca przypomina stolice innych europejskich miast. Nie ma tu też jako takiej starówki. Stolica Mołdawi.</p>   
      `
    ),
    new Interst(
      "Grecja",
      "podrozowanie/korfu_2018.jpg",
      new Date("2018-07-29"),
      new Date("2018-08-05"),
      `<p><strong>Korfu</strong> - górzysta wyspa w Grecji, w północnej części Morza Jońskiego u wybrzeży Albanii. Łącznie z kilkoma pobliskimi wysepkami tworzy gminę Korfu, w jednostce regionalnej Korfu, w regionie Wyspy Jońskie, w administracji zdecentralizowanej Peloponez, Grecja Zachodnia i Wyspy Jońskie</p> `
    ),
    ,
    new Interst(
      "Święty Krzyż - Krajno",
      "podrozowanie/swiety_krzyz_2018.jpg",
      new Date("2018-06-23"),
      new Date("2018-06-23"),
      `<p><strong>Święty Krzyż</strong> - odznacza się wybitnymi walorami krajobrazowymi. Od stuleci, zwarta, kamienna zabudowa szczytu góry pozostaje w niezmąconej relacji z otoczeniem - puszczą jodłową. Autentyczna jest także tzw. droga królewska - średniowieczny szlak prowadzący pielgrzymów z Nowej Słupi pod górę, do klasztoru.</p><br>
       <p><strong>Krajno</strong> - park Rozrywki i Miniatur Sabat Krajno to jedyny park miniatur w Europie usytuowany na wzgórzu, dzięki czemu znakomicie widać panoramę całego obiektu, połacie Świętokrzyskiego Parku Narodowwego oraz Łysicę, która towarzyszy nam podczas spaceru.`
    ),
    new Interst(
      "Kurozwęki",
      "podrozowanie/kurozweki_2018.jpg",
      new Date("2018-06-10"),
      new Date("2018-06-10"),
      `Pałac w Kurozwękach – kraina bizonów i niezwykłe labirynty z kukurydzy i konopi`
    ),
    new Interst(
      "Warszawa",
      "podrozowanie/warszawa_2018.jpg",
      new Date("2018-05-18"),
      new Date("2018-05-19"),
      `Jest jednocześnie największym miastem Polski i stolicą kraju. Położona jest w województwie mazowieckim, ma status miasta na prawach powiatu. Przez Warszawę przepływa najdłuższa rzeka Polski – Wisła. ... W mieście mieszczą się siedziby parlamentu, Rady Ministrów, Prezydenta oraz innych kluczowych władz w Polsce.`
    ),
    new Interst(
      "Kraków",
      "podrozowanie/krakow_bagry_2018.jpg",
      new Date("2018-04-21"),
      new Date("2018-04-22"),
      `<p><strong>Bagry</strong> - zbiornik wodny w Krakowie, powstały w wyniku zatopienia wyrobisk żwirowni. ... Jest jednym z większych zbiorników wodnych w granicach Krakowa. Do zalewu od strony południowej przylega stacja kolejowa Kraków Prokocim Towarowy, a nieco dalej na zachód znajduje się stacja Kraków Płaszów.</p>
      `
    ),
    new Interst(
      "Katowice - Ustroń",
      "podrozowanie/ustron_2018.jpg",
      new Date("2018-03-01"),
      new Date("2018-03-05"),
      `<p><strong>Katowice</strong> -  to największe miasto w województwie śląskim, w którym żyją ponad 303 tysiące osób. Jest on jednym z głównych ośrodków Górnośląskiego Okręgu Przemysłowego i siedzibą władz wojewódzkich. Początki miasta sięgają wieku XVI, kiedy to był on ośrodkiem kuźniczym i rolniczym.</p><br>
      <p><strong>Ustroń</strong> -  Ustroń to niewielkie, liczące ok. 16 tys. mieszkańców malownicze miasto w województwie śląskim, znane głównie jako uzdrowisko i ośrodek wypoczynkowo - rekreacyjny. Obecnie Ustroń jako jedyne miasto w Beskidzie Śląskim posiada status uzdrowiska.</p><br>
      `
    ),
  ]),
];

module.exports = interstsList;
