const ProjectModel = require("../models/ProjectsModel").ProjectsModel;
const ProjectModelLinks = require("../models/ProjectsModel").ProjectLinksModel;

const projectsList = [
  new ProjectModel(
    "Strona Portfolio",
    "projekty/portfolio.jpg",
    "Prosta strona napisana z wykorzystaniem technologii Angular",
    "HTML | Sass | Typescript | Angular | Angular Material | Node.js | npm | Express",
    new ProjectModelLinks("http://194.36.88.31/portfolio/about-me", "https://bitbucket.org/pawelkosowicz/about-me/")
  ),
  new ProjectModel(
    "Strona Dieta",
    "projekty/dieta.jpg",
    "Strona zwierajaca zestaw kalkulatorów obliczających zapotrzebowanie kaloryczne, wykorzystująca PWA",
    "HTML | Sass | Angular | Bootstrap | Node.js | npm | PWA",
    new ProjectModelLinks("https://pawelkosowicz.github.io/dieta/", "https://bitbucket.org/pawelkosowicz/diet-pwa")
  ),
];

module.exports = projectsList;
