const FeauturesModel = require('../models/FeaturesModel');

const featureMockList = [new FeauturesModel('Komunikatywność','posiadam łatwość nawiązywania relacji interpersonalnych, a także utrzymywania ich na wysokim poziomie'),
new FeauturesModel('Uczciwość','zawsze staram się wywiązywać z obietnic oraz dotrzymywać danego słowa'),
new FeauturesModel('Sumienność','do wypełniania swoich obowiązków podchodzę skrupulatnie z dbałością o szczegóły')];

module.exports = featureMockList;