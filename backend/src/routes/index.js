const express = require("express");
const router = express.Router();

const HomeController = require('../controllers/HomeController');
const ContactFormController = require('../controllers/ContactFormController');
const InterestsController = require('../controllers/InterestsController');
const FileController = require('../controllers/FileController');
const AboutMeController = require('../controllers/AboutMeController');
const ProjectsController = require('../controllers/ProjectsController');

const homeController = new HomeController();
const contactFormController = new ContactFormController();
const interestsController = new InterestsController();
const fileController = new FileController();
const aboutMeController = new AboutMeController();
const projectsController = new ProjectsController();

router.get("/", homeController.homePage);

router.post("/contactFormSend", contactFormController.sendForm);

router.get('/getAllInterest',interestsController.getAllInterests);
router.get('/getAllInterestsType',interestsController.getAllInterestsType);
router.get('/getInterest',interestsController.getInterest);

router.get('/getPdfFile',fileController.getPdfFile);

router.get('/getActually',aboutMeController.getActually);
router.get('/getOwnerData',aboutMeController.getOwnerData);
router.get('/getOwnerFeatures',aboutMeController.getOwnerFeatures);

router.get('/getAllProjects',projectsController.getAllProjects);

module.exports = router;