class PersonModel {
    constructor(name = '', email = '', adres = '', nationality = '', birthday = new Date()){
        this.name = name;
        this.email = email;
        this.adres = adres;
        this.nationality = nationality;
        this.birthday = birthday;
    }
}

module.exports = PersonModel;