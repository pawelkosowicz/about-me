class ProjectLinksModel {
    constructor(demo = '', code = '') {
        this.demo = demo;
        this.code = code;
    }
}

class ProjectsModel {
    constructor(title = '', image = '', description = '', technologies = '', links = new ProjectLinksModel()) {
        this.title = title;
        this.image = image;
        this.description = description;
        this.technologies = technologies;
        this.links = links;
    }
}

module.exports = {ProjectLinksModel, ProjectsModel }
