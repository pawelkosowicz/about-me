class ContactForm {

  constructor(contactForm) {
    this.name = contactForm.name;
    this.email = contactForm.email;
    this.subject = contactForm.subject;
    this.message = contactForm.message;
  }

}

module.exports = ContactForm;