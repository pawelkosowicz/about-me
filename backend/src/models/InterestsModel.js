class Interest {
  constructor(title, imagePath, fromDate, toDate, description, image = undefined) {
    this.title = title;
    this.imagePath = imagePath;
    this.image = image;
    this.fromDate = fromDate;
    this.toDate = toDate;
    this.description = description;
  }
}

class InterestsModel {
  constructor(type, interestList) {
    this.type = type;
    this.interestList = interestList;
  }
}

module.exports = { InterestsModel, Interest };
