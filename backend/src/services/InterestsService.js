const interstsList = require('../mocks/InterestsMock');
const fileModel = require('../models/FileModel');
const FileService = require('../services/FileService');

const fs = require('fs');
const path = require('path');

const fileService = new FileService();
const IMAGE_FILE_PATH = '../public/img/';


class InterstsService {
  constructor() {}

  getAllInterests() {
    return new Promise((resolve, reject) => {
      interstsList.forEach((interestArray) => {
        interestArray.interestList.map((interest)=> {
          interest.image = `data:image/jpg;base64,${fileService.getBase64(`${IMAGE_FILE_PATH}${interest.imagePath}`)}`;
        })
      })      
      resolve(interstsList);
    })
  }

  getInterests(interestType, year) {
    return new Promise((resolve, reject) => {
      const interest = Object.assign({},interstsList.find((interset)=> interset.type === interestType));
      interest.interestList = interest.interestList.filter(({fromDate})=>
        fromDate.getFullYear().toString() === year
      );

      interest.interestList.map((interest)=> {
          interest.image = `data:image/jpg;base64,${fileService.getBase64(`${IMAGE_FILE_PATH}${interest.imagePath}`)}`;
      });
      
      interest.interestList.sort((a,b)=> {
        return a.fromDate > b.fromDate? -1: 1;
      });   
      resolve(interest);
    })
  }

  getAllInterestsType() {
    return new Promise((resolve, reject) => {
      const interestTypes = interstsList.map((interests)=> interests.type);      
      resolve(interestTypes);
    })
  }

}

module.exports = InterstsService;
