const FileService = require('../services/FileService');
const projectsList = require('../mocks/ProjectsMock');

const fileService = new FileService();
const IMAGE_FILE_PATH = '../public/img/';

class ProjectsService {
    
  getAllProjects() {
    return new Promise((resolve, reject) => {
      const projectListObj = JSON.parse(JSON.stringify(projectsList));
        projectListObj.map((project)=> {
            project.image = `data:image/jpg;base64,${fileService.getBase64(`${IMAGE_FILE_PATH}${project.image}`)}`;
        })
        resolve(projectListObj);
    })
        
  }
}

module.exports = ProjectsService;
