const personMock = require('../mocks/PersonMock');
const featureMockList = require('../mocks/FeaturesMock');

class AboutMeService {
  constructor() {}

  getOwnerData() {
    return personMock;
  }

  getOwnerFeatures() {
    return featureMockList;
  }

  getActually() {
    return `<p>Początki mojej przygody z kodem sięgają szkoły średniej. Poprzez tworzenie prostych 
    aplikacji internetowych poznałem technologie takie jak HTML i CSS. W kolejnych latach próbowałem swoich sił zarówno 
    w technologiach Mobilnych, Backendowych jak i Frontentowych. W ostateczności całkowicie postanowiłem skupić się 
    na tworzeniu stron od strony wizualnej.</p><br><p> Aktualnie szukam pracy w IT jako Front-end Developer. Ponadto chętnie 
    wezmę udział w projektach typu open-source, szczególnie w JavaScript, Angular.</p>`;
  }
}

module.exports = AboutMeService;
