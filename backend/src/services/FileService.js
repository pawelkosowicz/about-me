const fs = require('fs');
const path = require('path');

const FILE_PATH = '../../files/';

class FileService {
    constructor(){}

    getPdfFile(name) {		
      const fileLocation = path.join(__dirname,`${FILE_PATH}${name}.pdf`);
      const data = fs.readFileSync(fileLocation);			
      return data;
    }    

    getBase64(filePath) {
      const fileLocation = path.join(__dirname,`${filePath}`);
      const data = fs.readFileSync(fileLocation,'base64');
      return data;
    }


}

module.exports = FileService;