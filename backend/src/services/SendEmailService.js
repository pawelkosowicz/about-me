require('dotenv').config();
const pug = require('pug');
const nodemailer = require("nodemailer");

class SendEmailService {

    constructor() { }

    async send(message = '') {   
        let transporter =  nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.EMAIL_SENDER, 
                pass: process.env.EMAIL_PASSWORD, 
            }, 
            tls: {
                rejectUnauthorized: false
            }
        });

        const payload = {
            to: process.env.EMAIL_RECEIVER,
            from: process.env.EMAIL_SENDER,
            ...message
        }

        // send mail with defined transport object
        return await transporter.sendMail(payload); 
    }    

    renderFile(payload,fileName = 'test') {
        return pug.renderFile(`${__dirname}/../views/${fileName}.pug`,payload);
    }
}

module.exports =  SendEmailService;
