const FileService = require('../services/FileService');
const fileService = new FileService();

class FileController {
    constructor(){}

    getPdfFile(req,res,next) {
        const {name} = req.query;

        if(typeof name !== 'undefined'){
            const pdfFile = fileService.getPdfFile(name);             
		    res.contentType("application/pdf");
            return res.status(200).send(pdfFile);
        } else {
            return res.status(404).send('error');
        }
        
    }
}


module.exports = FileController