const AboutMeService = require("../services/AboutMeService");
const aboutMeService = new AboutMeService();

class AboutMeController {
  constructor() {}

  getOwnerData(req,res,next) {
    const owner = aboutMeService.getOwnerData();
    if(typeof owner !== 'undefined') {
        res.status(200).send(JSON.stringify(owner));
    } else {
        res.status(404).send('error');
    }
  }

  getOwnerFeatures(req,res,next) {
    const features = aboutMeService.getOwnerFeatures();
    if(typeof features !== 'undefined') {
        res.status(200).send(JSON.stringify(features));
    } else {
        res.status(404).send('error');
    }
  }

  getActually(req,res,next) {
    const actually = aboutMeService.getActually();
    if(typeof actually !== 'undefined') {
        res.status(200).send(JSON.stringify(actually));
    } else {
        res.status(404).send('error');
    }
  }

}

module.exports = AboutMeController;