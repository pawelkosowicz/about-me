const InterestsService = require("../services/InterestsService");

class InterestController {
  constructor() {}

  getAllInterests(req, res, next) {
    const interestsService = new InterestsService();
    interestsService.getAllInterests().then((interstsList) => {
        res.status(200).send(interstsList);
    });
  }

  getInterest(req, res, next) {
    const {type, year} = req.query;
    if(typeof type !== 'undefined' && typeof year !== 'undefined') {
        const interestsService = new InterestsService();
        interestsService.getInterests(type,year).then((interests) => {
            res.status(200).send(interests);
        });
    }
    return res.status(500);
  }

  getAllInterestsType(req, res, next) {
    const interestsService = new InterestsService();
    interestsService.getAllInterestsType().then((interstTypes) => {
        res.status(200).send(interstTypes);
    });
  }

}

module.exports = InterestController;
