const ContactForm = require('../models/ContactForm');
const SendEmailService = require('../services/SendEmailService');

const sendEmail = new SendEmailService();

class ContactFormController {
  constructor() {}

  sendForm(req, res, next) {
    const contactForm = new ContactForm(req.body);

    const html = sendEmail.renderFile(contactForm,'test');

    const message = Object.assign({     
      subject: contactForm.subject,
      html
    })

    sendEmail.send(message).then((reponse)=> {  
       return res.status(200).send({'emailForm': contactForm.email});
    }).catch((e)=> {
      return res.status(500).send(e);
    })  

  }
}

module.exports = ContactFormController;
