const ProjectsService = require('../services/ProjectsService');

class ProjectsController {

    constructor() {}

    getAllProjects(req,res,next) {
        const projectsService = new ProjectsService();
        projectsService.getAllProjects().then(projectsList => {
            res.status(200).send(projectsList);
        },(error) => {
            res.status(500).send(error);
        })
    }
}


module.exports = ProjectsController;